package com.suhe.ozon.service;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suhe.ozon.dao.OrderProductDao;
import com.suhe.ozon.entity.po.OrderProductPO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *     订单中的商品Service
 * </p>
 *
 * @author liuning
 * @date 14:32 2024/1/19
 */
@Service
public class OrderProductService extends ServiceImpl<OrderProductDao, OrderProductPO> {

    /**
     * 保存或更新订单中的商品信息
     *
     * @param orderNo
     * @param orderProducts
     */
    @Transactional
    public void saveOrModify(String orderNo, List<OrderProductPO> orderProducts) {
        final List<OrderProductPO> oldOrderProducts = listByOrderNo(orderNo);
        if (CollectionUtil.isEmpty(oldOrderProducts)) { // 新增加的订单
            orderProducts.forEach(orderProduct -> this.save(orderProduct));
        } else { // 更新订单中的商品
            // 订单中已存在的商品sku
            final Set<String> oldProductSkus = oldOrderProducts.stream().map(OrderProductPO::getProductSku).collect(Collectors.toSet());
            // 当前订单中的商品sku
            final Set<String> newProductSkus = orderProducts.stream().map(OrderProductPO::getProductSku).collect(Collectors.toSet());

            // 需要删除的商品
            final Set<String> deleteProductSkus = oldProductSkus.stream().filter(p -> !newProductSkus.contains(p)).collect(Collectors.toSet());
            deleteProductSkus.forEach(productSku -> deleteByOrderNoAndProductSku(orderNo, productSku));
            // 需要新增的商品
            final Set<String> insertProductSkus = newProductSkus.stream().filter(p -> !oldProductSkus.contains(p)).collect(Collectors.toSet());
            orderProducts.stream().filter(p -> insertProductSkus.contains(p.getProductSku())).forEach(orderProduct -> this.save(orderProduct));
            // 需要更新的商品
            final Set<String> updateProductSkus = newProductSkus.stream().filter(p -> oldProductSkus.contains(p)).collect(Collectors.toSet());
            oldOrderProducts.stream().filter(p -> updateProductSkus.contains(p)).forEach(oldOrderProduct -> {
                final OrderProductPO newOrderProduct = orderProducts.stream().filter(p -> p.getProductSku().equals(oldOrderProduct.getProductSku())).findFirst().orElse(null);
                oldOrderProduct.conver(newOrderProduct);
                this.updateById(oldOrderProduct);
            });
        }
    }

    /**
     * 根据订单号获取订单下的商品信息
     *
     * @param orderNo
     * @return
     */
    public List<OrderProductPO> listByOrderNo(String orderNo) {
        return getBaseMapper().listByOrderNo(orderNo);
    }

    /**
     * 根据订单编号和商品货号删除订单中的商品
     *
     * @param orderNo
     * @param productSku
     */
    public void deleteByOrderNoAndProductSku(String orderNo, String productSku) {
        getBaseMapper().delete(Wrappers.lambdaQuery(OrderProductPO.class).eq(OrderProductPO::getOrderNo, orderNo).eq(OrderProductPO::getProductSku, productSku));
    }
}
