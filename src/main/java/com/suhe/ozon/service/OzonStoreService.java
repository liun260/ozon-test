package com.suhe.ozon.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suhe.ozon.dao.OzonStoreDao;
import com.suhe.ozon.entity.po.OzonStorePO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 14:28 2024/3/5
 */
@Service
public class OzonStoreService extends ServiceImpl<OzonStoreDao, OzonStorePO> {


}
