package com.suhe.ozon.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suhe.ozon.dao.ClientDao;
import com.suhe.ozon.entity.po.ClientPO;
import org.springframework.stereotype.Service;

/**
 * <p>
 *     客户端Service
 * </p>
 *
 * @author liuning
 * @date 14:43 2024/1/19
 */
@Service
public class ClientService extends ServiceImpl<ClientDao, ClientPO> {


}
