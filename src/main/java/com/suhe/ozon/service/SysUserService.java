package com.suhe.ozon.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suhe.ozon.dao.SysUserDao;
import com.suhe.ozon.entity.po.SysUserPO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 14:18 2024/3/5
 */
@Service
public class SysUserService extends ServiceImpl<SysUserDao, SysUserPO> {
}
