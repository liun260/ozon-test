package com.suhe.ozon.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suhe.ozon.dao.ProductDao;
import com.suhe.ozon.entity.po.ProductPO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *     商品Service
 * </p>
 *
 * @author liuning
 * @date 21:41 2024/1/20
 */
@Service
public class ProductService extends ServiceImpl<ProductDao, ProductPO> {

    public List<ProductPO> listByClientId(String clientId) {
        return getBaseMapper().selectList(Wrappers.lambdaQuery(ProductPO.class).eq(ProductPO::getClientId, clientId));
    }
}
