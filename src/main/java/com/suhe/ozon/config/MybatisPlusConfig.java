package com.suhe.ozon.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 *     Mybatis-Plus配置类
 * </p>
 *
 * @author liuning
 * @date 17:01 2024/1/19
 */
@Configuration
@MapperScan("com.suhe.ozon.dao")
public class MybatisPlusConfig {
}
