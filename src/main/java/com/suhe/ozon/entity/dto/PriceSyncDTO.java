package com.suhe.ozon.entity.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 23:45 2024/1/20
 */
@Data
@Accessors(chain = true)
public class PriceSyncDTO {

    /**
     * 最低价
     * 一个商品在应用促销活动后的最低价格
     */
    private BigDecimal minPrice;

    /**
     * 如果启用了商品的自动应用促销功能 —true。
     */
    private boolean autoActionEnabled;

}
