package com.suhe.ozon.entity.dto;

import com.suhe.ozon.entity.po.ProductPO;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 17:41 2024/1/21
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class PriceUpdateDTO {

    /**
     * 商品id
     */
    private String productId;

    /**
     * 最低价
     */
    private String minPrice;

    /**
     * 如果启用了商品的自动应用促销功能 —true。
     */
    private String autoActionEnabled;

    /**
     * 价格货币
     */
    private String currencyCode;

    /**
     * 售价
     */
    private String price;

    /**
     * 原价
     */
    private String oldPrice;

    /**
     * 用于自动应用价格策略的属性：
     *
     *     ENABLED — 启用;
     *     DISABLED — 关闭;
     *     UNKNOWN — 不做任何更改，默认赋值。
     *
     * 如果您之前已启用价格策略的自动应用，并且不希望关闭，请在后续请求中赋值 UNKNOWN 。
     *
     * 如果你在这个参数中提交ENABLED，在min_price参数中设置最低价格。
     *
     * 如果您在此参数中赋值 DISABLED，商品将从策略中移除。
     */
    private String priceStrategyEnabled = "UNKNOWN";

    public PriceUpdateDTO(ProductPO productPO, String autoActionEnabled) {
        this.setProductId(productPO.getOzonProductId());
        this.setOldPrice(productPO.getOldPrice().toString());
        this.setPrice(productPO.getPrice().toString());
        this.setMinPrice(productPO.getMinPrice().toString());
        this.setCurrencyCode(productPO.getCurrencyCode());
        this.setAutoActionEnabled(autoActionEnabled);
    }
}
