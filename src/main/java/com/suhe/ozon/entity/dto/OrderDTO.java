package com.suhe.ozon.entity.dto;

import com.suhe.ozon.entity.po.OrderPO;
import com.suhe.ozon.entity.po.OrderProductPO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 13:32 2024/1/19
 */
@Data
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class OrderDTO extends OrderPO {

    /**
     * 订单中的商品
     */
    private List<OrderProductPO> products = new ArrayList<>();
}
