package com.suhe.ozon.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *     商品信息
 * </p>
 *
 * @author liuning
 * @date 19:05 2024/1/20
 */
@Data
@Accessors(chain = true)
@TableName(value = "ozon_product", autoResultMap = true)
public class ProductPO {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 产品id
     */
    private String ozonProductId;

    /**
     * 客户端id
     */
    private String clientId;

    /**
     * 卖家系统中的商品识别码是商品货号
     */
    private String offerId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 原价
     * 考虑到折扣前的价格。在商品详情页上，价格被划掉。
     */
    private BigDecimal oldPrice;

    /**
     * 售价
     * 包括折扣在内的商品价格 —— 该值显示在商品详情页上
     */
    private BigDecimal price;

    /**
     * 促销活动价
     * 商品的价格，包括所有促销活动。这个值将在Ozon橱窗上显示
     */
    private BigDecimal marketingPrice;

    /**
     * 最低价
     * 一个商品在应用促销活动后的最低价格
     */
    private BigDecimal minPrice;

    /**
     * 如果启用了商品的自动应用促销功能 —true。
     *
     * ENABLED — 启用;
     * DISABLED — 关闭;
     * UNKNOWN — 不做任何更改，默认
     * 如果你以前启用了活动的自动应用，并且不想关闭它，请提交UNKNOWN。
     */
    private boolean autoActionEnabled;

    /**
     * 价格货币
     */
    private String currencyCode;

    /**
     * 商品主图url
     */
    private String primaryImage;

    /**
     * sku
     */
    private String sku;

    /**
     * 二维码
     */
    private String barcode;

    /**
     * 商品类型id
     */
    private String typeId;

    /**
     * 商品类别id
     */
    private String descriptionCategoryId;


    /**
     * 商品图片url
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> images = new ArrayList<>();

    /**
     * 如果该商品是出售的 —— true
     */
    private boolean visible;

    /**
     * 商品状态
     */
    private String state;

    /**
     * 商品库存
     */
    private int stock;

    /**
     * 商品创建时间
     */
    private LocalDateTime createdAt;

    /**
     * 商品上次更新时间
     */
    private LocalDateTime updatedAt;

}
