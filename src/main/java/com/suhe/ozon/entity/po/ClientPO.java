package com.suhe.ozon.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *     店铺信息
 * </p>
 *
 * @author liuning
 * @date 13:57 2024/1/19
 */
@Data
@Accessors(chain = true)
@TableName("ozon_client")
public class ClientPO {

    /**
     * 店铺id
     */
    @TableId(type = IdType.INPUT)
    private String clientId;

    /**
     * 秘钥
     */
    private String secretKey;

    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 绑定邮箱
     */
    private String email;

    /**
     * 所有人
     */
    private String owner;
}
