package com.suhe.ozon.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 *     订单中的商品信息
 * </p>
 *
 * @author liuning
 * @date 13:04 2024/1/19
 */
@Data
@Accessors(chain = true)
@TableName("ozon_order_product")
public class OrderProductPO {

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品单价
     */
    private BigDecimal productPrice;

    /**
     * 价格货币
     */
    private String productCurrencyCode;

    /**
     * 商品数量
     */
    private int productQuantity;

    /**
     * 商品sku
     */
    private String productSku;

    /**
     * 覆盖原来订单中的商品数据
     *
     * @param orderProductPO
     */
    public void conver(OrderProductPO orderProductPO) {
        this.setProductName(orderProductPO.getProductName())
                .setProductPrice(orderProductPO.getProductPrice())
                .setProductSku(orderProductPO.getProductSku())
                .setProductQuantity(orderProductPO.getProductQuantity())
                .setProductCurrencyCode(orderProductPO.getProductCurrencyCode());
    }
}
