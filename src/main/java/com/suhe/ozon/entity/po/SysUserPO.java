package com.suhe.ozon.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 14:16 2024/3/5
 */
@Data
@Accessors(chain = true)
@TableName(value = "sys_user", autoResultMap = true)
public class SysUserPO {

    private String id;

    private String account;

    private String name;

}
