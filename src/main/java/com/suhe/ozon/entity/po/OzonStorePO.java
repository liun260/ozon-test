package com.suhe.ozon.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *     Ozon门店
 * </p>
 *
 * @author liuning
 * @date 14:27 2024/3/5
 */
@Data
@Accessors(chain = true)
@TableName(value = "ozon_store", autoResultMap = true)
public class OzonStorePO {

    private String id;

    private String clientId;

    private String storeName;

    private String email;

    private String ownerId;
}
