package com.suhe.ozon.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *     订单
 * </p>
 *
 * @author liuning
 * @date 13:01 2024/1/19
 */
@Data
@Accessors(chain = true)
@TableName("ozon_order")
public class OrderPO {

    /**
     * 订单号
     */
    @TableId(type = IdType.INPUT)
    private String orderNo;

    /**
     * 下单时间
     */
    private LocalDateTime orderTime;

    /**
     * 订单价格
     */
    private BigDecimal orderPrice = BigDecimal.ZERO;

    /**
     * 商品总量
     */
    private int productQuantity;

    /**
     * 店铺id
     */
    private String clientId;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 物流跟踪号
     */
    private String trackingNumber;

    /**
     * 覆盖现有的订单数据
     *
     * @param orderPO
     */
    public void cover(OrderPO orderPO) {
        this.setOrderTime(orderPO.getOrderTime());
        this.setOrderPrice(orderPO.getOrderPrice());
        this.setClientId(orderPO.getClientId());
        this.setProductQuantity(orderPO.getProductQuantity());
        this.setOrderStatus(orderPO.getOrderStatus());
        this.setTrackingNumber(orderPO.getTrackingNumber());
    }
}
