package com.suhe.ozon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.suhe.ozon.entity.po.OrderPO;

/**
 * <p>
 *     订单Dao
 * </p>
 *
 * @author liuning
 * @date 13:13 2024/1/19
 */
public interface OrderDao extends BaseMapper<OrderPO> {

    /**
     * 根据订单编号获取订单信息
     *
     * @param orderNo
     * @return
     */
    default OrderPO getByOrderNo(String orderNo) {
        return this.selectOne(Wrappers.lambdaQuery(OrderPO.class).eq(OrderPO::getOrderNo, orderNo));
    }

}
