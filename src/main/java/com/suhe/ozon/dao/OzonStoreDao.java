package com.suhe.ozon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suhe.ozon.entity.po.OzonStorePO;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 14:28 2024/3/5
 */
public interface OzonStoreDao extends BaseMapper<OzonStorePO> {
}
