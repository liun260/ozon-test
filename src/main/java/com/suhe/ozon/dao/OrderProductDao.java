package com.suhe.ozon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.suhe.ozon.entity.po.OrderProductPO;

import java.util.List;

/**
 * <p>
 *     订单中的商品Dao
 * </p>
 *
 * @author liuning
 * @date 13:31 2024/1/19
 */
public interface OrderProductDao extends BaseMapper<OrderProductPO> {

    /**
     * 根据订单号获取订单下的商品信息
     *
     * @param orderNo
     * @return
     */
    default List<OrderProductPO> listByOrderNo(String orderNo) {
        return this.selectList(Wrappers.lambdaQuery(OrderProductPO.class).eq(OrderProductPO::getOrderNo, orderNo));
    }

}
