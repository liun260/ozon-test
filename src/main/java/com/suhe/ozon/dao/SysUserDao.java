package com.suhe.ozon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suhe.ozon.entity.po.SysUserPO;

/**
 * <p>
 * </p>
 *
 * @author liuning
 * @date 14:17 2024/3/5
 */
public interface SysUserDao extends BaseMapper<SysUserPO> {
}
