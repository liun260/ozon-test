package com.suhe.ozon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suhe.ozon.entity.po.ClientPO;

/**
 * <p>
 *     客户端Dao
 * </p>
 *
 * @author liuning
 * @date 13:59 2024/1/19
 */
public interface ClientDao extends BaseMapper<ClientPO> {
}
