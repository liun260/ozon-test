package com.suhe.ozon.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suhe.ozon.entity.po.ProductPO;

/**
 * <p>
 *     商品Dao
 * </p>
 *
 * @author liuning
 * @date 21:40 2024/1/20
 */
public interface ProductDao extends BaseMapper<ProductPO> {
}
