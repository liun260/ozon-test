package com.suhe.ozon.job;

import cn.hutool.core.util.StrUtil;
import com.suhe.ozon.entity.dto.OrderDTO;
import com.suhe.ozon.entity.po.ClientPO;
import com.suhe.ozon.entity.po.OrderPO;
import com.suhe.ozon.http.OrderHttp;
import com.suhe.ozon.service.ClientService;
import com.suhe.ozon.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *     订单同步任务
 * </p>
 *
 * @author liuning
 * @date 13:55 2024/1/19
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class OrderSyncJob {

    /**
     * 订单状态
     *
     * 文档地址: https://docs.ozon.ru/api/seller/zh/#tag/FBS
     * awaiting_registration — 等待注册，
     * acceptance_in_progress — 正在验收，
     * awaiting_approve — 等待确认，
     * awaiting_packaging — 等待包装，
     * awaiting_deliver — 等待装运，
     * arbitration — 仲裁，
     * client_arbitration — 快递客户仲裁，
     * delivering — 运输中，
     * driver_pickup — 司机处，
//     * cancelled — 已取消，
     * not_accepted — 分拣中心未接受，
//     * sent_by_seller – 由卖家发送
     */
    private final static String[] ORDER_STATUS = new String[]{
            "awaiting_registration"
            , "acceptance_in_progress"
            , "awaiting_approve"
            , "awaiting_packaging"
            , "awaiting_deliver"
            , "arbitration"
            , "client_arbitration"
            , "delivering"
            , "driver_pickup"
            /*, "cancelled"*/
            , "not_accepted"
//            , "sent_by_seller"
    };


    /**
     * 定时任务执行
     */
    public void start() {
        allOrder(null);
    }

    /**
     * 同步所有订单
     */
    public void allOrder(String clientId) {
        Arrays.asList(ORDER_STATUS).forEach(orderStatus -> {
            process(clientId, orderStatus);
        });
    }

    /**
     * 同步指定状态的订单
     *
     * @param orderStatus
     */
    public void process(String clientId, String orderStatus) {

        // 获取所有客户端
        final List<ClientPO> clientPOS = clientService.list().stream().filter(p -> StrUtil.isBlank(clientId) || p.getClientId().equals(clientId)).collect(Collectors.toList());
        clientPOS.forEach(client -> {
            // 获取最新一个订单
//            final OrderPO lastOrderPO = orderService.getLastOrder(client.getClientId());
            final OrderPO lastOrderPO = null;
            try {
                final List<OrderDTO> orderDTOS = new ArrayList<>();
                new OrderHttp().sync(client, lastOrderPO == null ? null : lastOrderPO.getOrderTime(), orderStatus, orderDTOS);
                orderDTOS.forEach(order -> {
                    try {
                        orderService.saveOrModify(order, client);
                    } catch (MessagingException e) {
                        log.error("订单更新失败", e);
                    }
                });
            } catch (IOException e) {
                log.error("同步订单数据失败, clientId = {}", client.getClientId(), e);
            }
        });
    }

    private final ClientService clientService;

    private final OrderService orderService;

}
