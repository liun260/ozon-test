package com.suhe.ozon.consts;

/**
 * <p>
 *     常量类
 * </p>
 *
 * @author liuning
 * @date 22:06 2024/1/20
 */
public class OzonConsts {

    public final static int HTTP_PAGE_LIMIT = 500;
}
