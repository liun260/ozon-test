//package com.suhe.ozon;
//
//import org.apache.pdfbox.pdmodel.PDDocument;
//import org.apache.pdfbox.text.PDFTextStripperByArea;
//import org.apache.pdfbox.text.TextPosition;
//
//import java.awt.*;
//import java.io.IOException;
//import java.util.List;
//
///**
// * <p>
// * </p>
// *
// * @author liuning
// * @date 10:17 2024/3/26
// */
//public class PdfEdit {
//
//    public static void replaceText(String pdfPath, String outputPath, String searchString, String replaceString) throws IOException {
//        PDDocument document = PDDocument.load(pdfPath);
//
//        PDFTextStripperByArea stripper = new PDFTextStripperByArea() {
//            @Override
//            protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
//                string = string.replace(searchString, replaceString);
//                super.writeString(string, textPositions);
//            }
//        };
//
//        stripper.setSortByPosition(true);
//        stripper.addRegion("textRegion", new Rectangle(1, 1, 1000, 1000));
//
//        stripper.extractRegions(document);
//        stripper.writeText(document, outputPath);
//
//        document.close();
//    }
//
//    public static void main(String[] args) {
//        try {
//            replaceText("input.pdf", "output.pdf", "oldText", "newText");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//}
