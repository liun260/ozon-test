//package com.suhe.ozon;
//
//import com.itextpdf.kernel.colors.ColorConstants;
//import com.itextpdf.kernel.geom.Rectangle;
//import com.itextpdf.kernel.pdf.PdfDocument;
//import com.itextpdf.kernel.pdf.PdfReader;
//import com.itextpdf.kernel.pdf.PdfWriter;
//import com.itextpdf.kernel.pdf.canvas.parser.PdfCanvasProcessor;
//import com.itextpdf.kernel.pdf.canvas.parser.listener.IEventListener;
//import com.itextpdf.kernel.pdf.canvas.parser.listener.LocationTextExtractionStrategy;
//import com.itextpdf.layout.Document;
//import com.itextpdf.layout.element.Paragraph;
//import com.itextpdf.layout.property.VerticalAlignment;
//
//import java.io.File;
//import java.io.IOException;
//
///**
// * <p>
// * </p>
// *
// * @author liuning
// * @date 10:22 2024/3/26
// */
//public class PdfEdit2 {
//
//    public static void replaceTextInPDF(String src, String dest, String searchString, String replaceString) throws IOException {
//        PdfReader reader = new PdfReader(src);
//        PdfWriter writer = new PdfWriter(dest);
//        PdfDocument pdfDoc = new PdfDocument(reader, writer);
//        Document doc = new Document(pdfDoc);
//
//        // Extract text from the PDF
//        IEventListener listener = new LocationTextExtractionStrategy();
//        PdfCanvasProcessor processor = new PdfCanvasProcessor(listener);
//        for (int i = 1; i <= pdfDoc.getNumberOfPages(); i++) {
//            Rectangle pageSize = pdfDoc.getPage(i).getPageSize();
////            processor.processPageContent(pdfDoc.getPage(i), pageSize);
//            processor.processPageContent(pdfDoc.getPage(i));
//        }
//
//        // Replace the text
//        String text = ((LocationTextExtractionStrategy) listener).getResultantText();
//        text = text.replace(searchString, replaceString);
//
//        // Clear the existing content and add the modified text
//        for (int i = 1; i <= pdfDoc.getNumberOfPages(); i++) {
//            Paragraph p = new Paragraph(text).setFixedPosition(100, 700, i).setVerticalAlignment(VerticalAlignment.TOP);
//            doc.add(p);
//        }
//
//        doc.close();
//    }
//
//    public static void main(String[] args) {
//        try {
//            replaceTextInPDF("/Users/ningdd/Downloads/4b648e08-b18e-413e-8567-4d9a37331d5b.pdf", "/Users/ningdd/Downloads/1.pdf", "Подарочный набор 礼品套装", "игрушка 玩具");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//}
