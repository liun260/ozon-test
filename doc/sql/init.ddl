drop table if exists ozon_client;
create table ozon_client (
    client_id varchar(32) not null primary key comment 'ozon客户端id',
    secret_key varchar(64) not null comment '秘钥',
    store_name varchar(526) not null comment '店铺名称',
    email varchar(128) not null comment '店铺绑定的邮箱',
    owner varchar(128) not null comment '所属人'
) comment 'ozon店铺';

drop table if exists ozon_order;
create table ozon_order (
    order_no varchar(32) not null primary key comment '订单编号',
    order_time datetime not null comment '下单时间',
    order_price decimal(10, 2) not null comment '订单总价',
    product_quantity int not null comment '商品数量',
    order_status varchar(64) not null comment '订单状态',
    client_id varchar(32) not null comment '客户端id',
    index ozon_order_client_id(client_id)
) comment 'ozon订单';

drop table if exists ozon_order_product;
create table ozon_order_product (
    id bigint not null primary key comment '主键',
    order_no varchar(32) not null comment '订单编号',
    product_name varchar(512) comment '商品名称',
    product_price decimal(10, 2) comment '商品价格',
    product_currency_code varchar(10) comment '价格货币',
    product_quantity int not null comment '商品数量',
    product_sku varchar(128) not null comment '商品sku',
    index ozon_order_product_order_no(order_no),
    index ozon_order_product_offer_id(product_offer_id)
) comment 'ozon订单中商品信息';

drop table if exists ozon_product;
create table ozon_product (
    product_id varchar(32) not null primary key comment '产品id',
    client_id varchar(32) not null comment '客户端id',
    offer_id varchar(128) not null comment '卖家系统中的商品识别码是商品货号',
    name varchar(128) not null comment '商品名称',
    old_price decimal(10, 2) not null comment '原价',
    price decimal(10, 2) not null comment '售价',
    marketing_price decimal(10, 2) not null comment '促销价',
    min_price decimal(10, 2) comment '最低价',
    auto_action_enabled boolean not null comment '如果启用了商品的自动应用促销功能 —true',
    currency_code varchar(8) not null comment '价格货币',
    primary_image varchar(256) not null comment '主图url',
    sku varchar(64) not null comment 'sku',
    barcode varchar(64) comment '二维码',
    type_id varchar(64) not null comment '商品类型id',
    description_category_id varchar(64) not null comment '商品类别id',
    images json comment '商品图片url',
    visible boolean not null comment '如果该商品是出售的 —— true',
    state varchar(32) not null comment '商品状态',
    stock int not null comment '商品库存',
    created_at datetime not null comment '商品创建时间',
    updated_at datetime not null comment '商品上次更新时间'
) comment '商品信息';
